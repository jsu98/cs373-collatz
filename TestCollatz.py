#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_2(self):
        v = collatz_eval(18, 4)
        self.assertEqual(v, 21)

    def test_eval_3(self):
        v = collatz_eval(1, 1000)
        self.assertEqual(v, 179)

    def test_eval_4(self):
        v = collatz_eval(1, 3000)
        self.assertEqual(v, 217)

    def test_eval_5(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(501, 499)
        self.assertEqual(v, 111)

    def test_eval_8(self):
        v = collatz_eval(500, 501)
        self.assertEqual(v, 111)

    def test_eval_9(self):
        v = collatz_eval(131597, 378056)
        self.assertEqual(v, 443)

    def test_eval_10(self):
        v = collatz_eval(29787, 822544)
        self.assertEqual(v, 509)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO(
            "1 1\n18 4\n1 1000\n1 3000\n3000 6\n1 999999\n501 499\n"
            + "500 501\n131597 378056\n29787 822544"
        )
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "1 1 1\n18 4 21\n1 1000 179\n1 3000 217\n3000 6 217\n1 999999 525\n501 499 111\n"
            + "500 501 111\n131597 378056 443\n29787 822544 509\n",
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
